package ua.uraty.utils.mongo

import java.util.concurrent.ConcurrentHashMap

import com.mongodb.client.model.ReturnDocument
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.model.{Filters, FindOneAndUpdateOptions, Updates}

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

/**
  * @author Dmitry Barannik
  * @since 13.02.18
  */
class MongoId private (database: MongoDatabase) {

  object CounterSchema {
    val name = "_id"
    val value = "value"
  }
  private val countersCollection = database.getCollection("counters")

  def generateId(counter: String)
                (implicit context: ExecutionContext): Future[Long] = {
    countersCollection
      .findOneAndUpdate(Filters.eq(CounterSchema.name, counter),
        Updates.inc(CounterSchema.value, 1L), FindOneAndUpdateOptions().upsert(true).returnDocument(ReturnDocument.AFTER)
      )
      .toFuture()
      .map(_.getLong(CounterSchema.value))
  }

  def withIdFor[T](counter: String)
                  (idConsumer: Long => Future[T])
                  (implicit context: ExecutionContext): Future[T] =
    generateId(counter).flatMap(idConsumer)

  def mapIdFor[T](counter: String)
                  (idConsumer: Long => T)
                  (implicit context: ExecutionContext): Future[T] =
    generateId(counter).map(idConsumer)
}

object MongoId {
  private val existedGenerators = new ConcurrentHashMap[String, MongoId]()

  def apply(database: MongoDatabase): MongoId =
    existedGenerators.computeIfAbsent(database.name, _ => new MongoId(database))
}