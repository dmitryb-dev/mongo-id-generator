package ua.uratyu.utils.mongo

import java.util
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.AtomicReference

import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.{MongodConfigBuilder, Net}
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network
import org.mongodb.scala.MongoClient
import org.scalatest.{FlatSpec, Matchers}
import ua.uraty.utils.mongo.MongoId

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Failure

/**
  * @author Dmitry Barannik
  * @since 13.02.18
  */
class Test extends FlatSpec with Matchers {

  import scala.concurrent.ExecutionContext.Implicits.global

  it should "generate id sequence in db" in {
    withMongo { implicit client =>

      val error = new AtomicReference[Throwable](null)

      val expectedTestC1 = new CopyOnWriteArrayList[Long](util.Arrays.asList(1L, 2L, 3L, 4L))
      for (_ <- 1 to 2) testGeneration("test", "c1", error, expectedTestC1)

      val expectedTestC2 = new CopyOnWriteArrayList[Long](util.Arrays.asList(1L, 2L, 3L))
      for (_ <- 1 to 3) testGeneration("test", "c2", error, expectedTestC2)

      val expectedTest2C1 = new CopyOnWriteArrayList[Long](util.Arrays.asList(1L, 2L, 3L, 4L, 5L))
      for (_ <- 1 to 5) testGeneration("test2", "c1", error, expectedTest2C1)

      // Continue of expectedTestC1
      for (_ <- 1 to 2) testGeneration("test", "c1", error, expectedTestC1)

      waitFor(5 seconds) {
        expectedTestC1.size() == 0 &&
        expectedTestC2.size() == 0 &&
        expectedTest2C1.size() == 0 ||
        error.get != null
      }
      if (error.get != null) {
        throw error.get
      }
    }
  }

  def testGeneration(db: String, counter: String, errorContainer: AtomicReference[Throwable], expectedSequence: util.List[Long])
                    (implicit client: MongoClient) = {
    MongoId(client.getDatabase(db)).mapIdFor(counter) { id =>
      withClue(s"$db:$counter; No place for id = $id in list $expectedSequence") {
        expectedSequence.remove(Long.box(id)) should be(true)
      }
    }.exposeException(errorContainer)
  }

  def withMongo(action: MongoClient => Any) = {
    val starter = MongodStarter.getDefaultInstance
    val address = "localhost"
    val port = 12345
    val mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION).net(new Net(address, port, Network.localhostIsIPv6)).build

    val mongodExecutable = starter.prepare(mongodConfig)
    val mongod = mongodExecutable.start

    try {
      action(MongoClient(s"mongodb://$address:$port"))
    } catch {
      case e: Throwable => throw e
    } finally {
      mongod.stop()
    }
  }

  def waitFor(timeout: Duration)(condition: => Boolean) = {
    val startTime = System.currentTimeMillis()
    while (System.currentTimeMillis() - startTime < timeout.toMillis && !condition) {
      Thread.`yield`()
    }
    if (!condition) throw new AssertionError("Timeout")
  }

  implicit class ExceptionExposing[T](future: Future[T]) {
    def exposeException(container: AtomicReference[Throwable]) =
      future onComplete {
        case Failure(e) => if (container.get == null) container.set(e)
        case _ =>
      }
  }
}
